# GitHub user repositories statistics API
Simple RESTful service built with ASP.NET Core 3.1 including docker support and e2e tests.  
The service allows for computing Github repositories statistics for a particular user.

## Prerequisites
- [Docker Desktop](https://www.docker.com/products/docker-desktop)

## How to Test
```
docker-compose build e2e-tests
docker-compose up e2e-tests
docker-compose down
```

## How to Run
```
docker-compose build web-service
docker-compose up web-service
docker-compose down
```
Navigate to ```http://localhost:7200/repositories/{owner}```

To access private repositories or increase rate limit run the service on behalf of an authenticated user
by adding OAuth2 token to **secret.txt**
```
echo {token} > secret.txt
```
