
using System.Threading.Tasks;
using WK.GitHubStats.Core.Models;

namespace WK.GitHubStats.Core.Services
{
  public interface IStatistics
  {
    Task<Statistics> GetAsync( string owner );
  }
}