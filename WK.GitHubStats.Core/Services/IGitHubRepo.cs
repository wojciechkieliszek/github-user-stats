
using System.Collections.Generic;
using System.Threading.Tasks;
using WK.GitHubStats.Core.Models;

namespace WK.GitHubStats.Core.Services
{
  public interface IGitHubRepo
  {
    Task<IEnumerable<GitHubRepo>> ListAsync( string owner );
  }
}