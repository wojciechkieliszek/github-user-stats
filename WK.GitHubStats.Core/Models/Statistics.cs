using System.Collections.Generic;

namespace WK.GitHubStats.Core.Models
{
  public class Statistics
  {
    public string Owner { get; set; }
    public IDictionary<char, int> Letters { get; set; }
    public int AvgStargazers { get; set; }
    public int AvgWatchers { get; set; }
    public int AvgForks { get; set; }
    public long AvgSize { get; set; }
  }
}