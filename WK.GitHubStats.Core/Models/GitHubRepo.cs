namespace WK.GitHubStats.Core.Models
{
  public class GitHubRepo
  {
    public string Name { get; set; }
    public int StargazersCount { get; set; }
    public int WatchersCount { get; set; }
    public int ForksCount { get; set; }
    public long Size { get; set; }
  }
}