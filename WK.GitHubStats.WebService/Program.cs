using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace WK.GitHubStats.WebService
{
  public class Program
  {
    public static void Main( string[ ] args )
    {
      CreateHostBuilder( args ).Build( ).Run( );
    }

    public static IHostBuilder CreateHostBuilder( string[ ] args ) =>
      Host.CreateDefaultBuilder( args )
          .ConfigureAppConfiguration( ( hostingContext, config ) =>
          {
            config.AddKeyPerFile( "/run/secrets", true );
          } )
          .ConfigureWebHostDefaults( webBuilder =>
          {
            webBuilder.UseStartup<Startup>( );
          } );
  }
}
