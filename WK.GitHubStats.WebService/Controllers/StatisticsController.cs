﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WK.GitHubStats.Core.Models;
using WK.GitHubStats.Core.Services;

namespace WK.GitHubStats.WebService.Controllers
{
  [ApiController]
  [Route( "repositories" )]
  public class StatisticsController : ControllerBase
  {
    private readonly IStatistics _statisticsService;

    public StatisticsController( IStatistics statisticsService )
    {
      _statisticsService = statisticsService;
    }

    [HttpGet( "{owner}" )]
    public Task<Statistics> GetAsync( string owner ) => _statisticsService.GetAsync( owner );
  }
}
