using System;
using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Octokit;
using WK.GitHubStats.Core.Services;
using WK.GitHubStats.Services;

namespace WK.GitHubStats.WebService
{
  public class Startup
  {
    public Startup( IConfiguration configuration )
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices( IServiceCollection services )
    {
      services
      .AddControllers( )
      .AddNewtonsoftJson( options =>
       {
         options.SerializerSettings.Formatting = Formatting.Indented;
       } );

      services.AddHealthChecks( );

      services.AddSingleton<IConnection>( sp =>
      {
        var appName = Configuration.GetValue<string>( "GitHubRepoServiceOptions:AppName" );
        var token = Configuration.GetValue<string>( "github-token" );

        var connection = new Connection( new ProductHeaderValue( appName ) );

        if( !string.IsNullOrWhiteSpace( token ) )
          connection.Credentials = new Credentials( token );

        return connection;
      } );

      services.AddTransient<IApiConnection, ApiConnection>( );
      services.AddTransient<IGitHubRepo, GitHubRepoService>( );
      services.AddTransient<IStatistics, StatisticsService>( );
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure( IApplicationBuilder app, IWebHostEnvironment env )
    {
      if( env.IsDevelopment( ) )
      {
        app.UseDeveloperExceptionPage( );
      }
      else
      {
        app.UseExceptionHandler( errorApp =>
        {
          errorApp.Run( async context =>
            {
              var contextFeature = context.Features.Get<IExceptionHandlerFeature>( );

              context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
              context.Response.ContentType = "application/json";

              if( contextFeature?.Error is ArgumentOutOfRangeException )
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;

              if( contextFeature?.Error is ArgumentNullException )
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;

              var result = JsonConvert.SerializeObject( new { message = contextFeature?.Error?.Message } );
              await context.Response.WriteAsync( result );
            } );
        } );
      }

      app.UseRouting( );

      app.UseAuthorization( );

      app.UseEndpoints( endpoints =>
       {
         endpoints.MapControllers( );
         endpoints.MapHealthChecks( "/health" );
       } );
    }
  }
}
