FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /src
COPY WK.GitHubStats.sln .
COPY WK.GitHubStats.Core/WK.GitHubStats.Core.csproj WK.GitHubStats.Core/
COPY WK.GitHubStats.Services/WK.GitHubStats.Services.csproj WK.GitHubStats.Services/
COPY WK.GitHubStats.WebService/WK.GitHubStats.WebService.csproj WK.GitHubStats.WebService/
COPY tests/WK.GitHubStats.Tests.Unit/WK.GitHubStats.Tests.Unit.csproj tests/WK.GitHubStats.Tests.Unit/
COPY tests/WK.GitHubStats.Tests.E2e/WK.GitHubStats.Tests.E2e.csproj tests/WK.GitHubStats.Tests.E2e/

RUN dotnet restore

COPY . .
WORKDIR /src/WK.GitHubStats.WebService
RUN dotnet publish -c Release -o /out

FROM build-env AS test-env
WORKDIR /src/tests/WK.GitHubStats.Tests.Unit
RUN dotnet test

FROM build-env AS test-runner-env
WORKDIR /src/tests/WK.GitHubStats.Tests.E2e
ENTRYPOINT ["dotnet", "test"]

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
EXPOSE 80
COPY --from=build-env /out .
ENTRYPOINT ["dotnet", "WK.GitHubStats.WebService.dll"]
