using System;
using System.Collections.Generic;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using WK.GitHubStats.Core.Models;
using WK.GitHubStats.Core.Services;
using WK.GitHubStats.Services;
using Xunit;

namespace WK.GitHubStats.Tests.Unit
{
  public class StatisticsServiceTest
  {
    private readonly Dictionary<string, IEnumerable<GitHubRepo>> testRepo
      = new Dictionary<string, IEnumerable<GitHubRepo>>
      {
         { "testOwner1", new GitHubRepo[ ] {  } },
         { "testOwner2", new[ ] {
           new GitHubRepo { Name = "test", StargazersCount = 5, WatchersCount = 4, ForksCount = 3, Size = 34 },
           new GitHubRepo { Name = "test2", StargazersCount = 7, WatchersCount = 11, ForksCount = 4, Size = 12 } }
         }
      };

    private readonly Dictionary<string, Statistics> testStatistics
     = new Dictionary<string, Statistics>  {
      { "testOwner1", new Statistics {
        Owner = "testOwner1",
        AvgStargazers = 0,
        AvgWatchers = 0,
        AvgForks = 0,
        AvgSize = 0,
        Letters = new Dictionary<char, int> {
          { 'a', 0 }, { 'b', 0 }, { 'c', 0 }, { 'd', 0 }, { 'e', 0 },
          { 'f', 0 }, { 'g', 0 }, { 'h', 0 }, { 'i', 0 }, { 'j', 0 },
          { 'k', 0 }, { 'l', 0 }, { 'm', 0 }, { 'n', 0 }, { 'o', 0 },
          { 'p', 0 }, { 'q', 0 }, { 'r', 0 }, { 's', 0 }, { 't', 0 },
          { 'u', 0 }, { 'v', 0 }, { 'w', 0 }, { 'x', 0 }, { 'y', 0 },
          { 'z', 0 } } } },
      { "testOwner2", new Statistics {
        Owner = "testOwner2",
        AvgStargazers = 6,
        AvgWatchers = 8,
        AvgForks = 4,
        AvgSize = 23,
        Letters = new Dictionary<char, int> {
          { 'a', 0 }, { 'b', 0 }, { 'c', 0 }, { 'd', 0 }, { 'e', 2 },
          { 'f', 0 }, { 'g', 0 }, { 'h', 0 }, { 'i', 0 }, { 'j', 0 },
          { 'k', 0 }, { 'l', 0 }, { 'm', 0 }, { 'n', 0 }, { 'o', 0 },
          { 'p', 0 }, { 'q', 0 }, { 'r', 0 }, { 's', 2 }, { 't', 4 },
          { 'u', 0 }, { 'v', 0 }, { 'w', 0 }, { 'x', 0 }, { 'y', 0 },
          { 'z', 0 } } }
        } };

    private IStatistics CreateStatisticsService( )
    {
      var mockRepo = new Mock<IGitHubRepo>( );
      mockRepo.Setup( repo => repo.ListAsync( It.IsAny<string>( ) ) )
        .ReturnsAsync( ( string owner ) => testRepo[ owner ] );

      var mockLogger = new Mock<ILogger<StatisticsService>>( );

      var service = new StatisticsService( mockLogger.Object, mockRepo.Object );

      return service;
    }

    [Theory]
    [InlineData( null )]
    [InlineData( "" )]
    [InlineData( "  " )]
    public void GetAsync_NullOrEmptyOwner_ReturnsException( string owner )
    {
      var service = CreateStatisticsService( );

      Assert.ThrowsAsync<ArgumentNullException>( ( ) => service.GetAsync( owner ) );
    }

    [Theory]
    [InlineData( "testOwner1" )]
    [InlineData( "testOwner2" )]
    public async void GetAsync_OwnerWithRepos_ReturnsCorrectStatistics( string owner )
    {
      var service = CreateStatisticsService( );
      var expected = testStatistics[ owner ];

      var actual = await service.GetAsync( owner );

      actual.Should( ).BeEquivalentTo( expected );
    }
  }
}