using System.Collections.Generic;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using WK.GitHubStats.Core.Models;
using Xunit;

namespace WK.GitHubStats.Tests.E2e
{
  public class E2eTests : IClassFixture<WebApplicationFactory<WK.GitHubStats.WebService.Startup>>
  {
    private readonly WebApplicationFactory<WK.GitHubStats.WebService.Startup> _factory;

    public E2eTests( WebApplicationFactory<WK.GitHubStats.WebService.Startup> factory )
    {
      _factory = factory;
    }

    [Fact]
    public async void GetRepositories_CorrectStatusCodeAndContentType( )
    {
      var client = _factory.CreateClient( );
      var expected = "application/json; charset=utf-8";

      var response = await client.GetAsync( "/repositories/kielosz" );
      response.EnsureSuccessStatusCode( );
      var actual = response.Content.Headers.ContentType.ToString( );

      Assert.Equal( expected, actual );
    }

    [Fact]
    public async void GetRepositories_CorrectResponse( )
    {
      var client = _factory.CreateClient( );
      var expected = new Statistics
      {
        Owner = "kielosz",
        AvgStargazers = 1,
        AvgWatchers = 0,
        AvgForks = 0,
        AvgSize = 85633,
        Letters = new Dictionary<char, int>
        {
          { 'a', 0 }, { 'b', 0 }, { 'c', 1 }, { 'd', 2 }, { 'e', 4 },
          { 'f', 0 }, { 'g', 0 }, { 'h', 0 }, { 'i', 0 }, { 'j', 0 },
          { 'k', 0 }, { 'l', 0 }, { 'm', 0 }, { 'n', 1 }, { 'o', 3 },
          { 'p', 1 }, { 'q', 0 }, { 'r', 1 }, { 's', 2 }, { 't', 4 },
          { 'u', 0 }, { 'v', 1 }, { 'w', 0 }, { 'x', 0 }, { 'y', 0 },
          { 'z', 0 }
        }
      };

      var response = await client.GetAsync( "/repositories/kielosz" );
      var jsonData = await response.EnsureSuccessStatusCode( ).Content.ReadAsStringAsync( );
      var actual = JsonConvert.DeserializeObject<Statistics>( jsonData );

      actual.Should( ).BeEquivalentTo( expected );
    }
  }
}
