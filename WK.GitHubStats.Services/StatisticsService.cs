using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using WK.GitHubStats.Core.Models;
using WK.GitHubStats.Core.Services;

namespace WK.GitHubStats.Services
{
  public class StatisticsService : IStatistics
  {
    private readonly ILogger _logger;
    private readonly IGitHubRepo _gitHubRepoService;

    public StatisticsService( ILogger<StatisticsService> logger, IGitHubRepo gitHubRepoService )
    {
      _logger = logger;
      _gitHubRepoService = gitHubRepoService;
    }

    public async Task<Statistics> GetAsync( string owner )
    {
      if( string.IsNullOrWhiteSpace( owner ) )
        throw new ArgumentNullException( nameof( owner ) );

      var userRepos = await _gitHubRepoService.ListAsync( owner );

      var avgStargazers = (int)Math.Round( userRepos.Select( r => r.StargazersCount ).DefaultIfEmpty( ).Average( ) );
      var avgWatchers = (int)Math.Round( userRepos.Select( r => r.WatchersCount ).DefaultIfEmpty( ).Average( ) );
      var avgForks = (int)Math.Round( userRepos.Select( r => r.ForksCount ).DefaultIfEmpty( ).Average( ) );
      var avgSize = (long)Math.Round( userRepos.Select( r => r.Size ).DefaultIfEmpty( ).Average( ) );

      var charDict = new Dictionary<char, int>( );
      for( int i = (int)'a'; i <= (int)'z'; i++ )
        charDict.Add( (char)i, 0 );

      foreach( var repo in userRepos )
        foreach( var @char in repo.Name )
        {
          var lowerChar = char.ToLower( @char );
          if( charDict.ContainsKey( lowerChar ) )
            charDict[ lowerChar ]++;
        }

      var result = new Statistics
      {
        Owner = owner,
        Letters = charDict,
        AvgStargazers = avgStargazers,
        AvgWatchers = avgWatchers,
        AvgForks = avgForks,
        AvgSize = avgSize
      };

      return result;
    }
  }
}