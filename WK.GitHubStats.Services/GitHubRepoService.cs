using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Octokit;
using WK.GitHubStats.Core.Models;
using WK.GitHubStats.Core.Services;

namespace WK.GitHubStats.Services
{
  public class GitHubRepoService : IGitHubRepo
  {
    private readonly ILogger _logger;
    private readonly IApiConnection _client;

    public GitHubRepoService( ILogger<GitHubRepoService> logger, IApiConnection client )
    {
      _logger = logger;
      _client = client;
    }

    public async Task<IEnumerable<GitHubRepo>> ListAsync( string owner )
    {
      IEnumerable<Repository> userRepos;

      try
      {
        userRepos = await _client.GetAll<Repository>( ApiUrls.Repositories( owner ), null,
          AcceptHeaders.LicensesApiPreview, ApiOptions.None );
      }
      catch( Octokit.NotFoundException exception )
      {
        this._logger.LogInformation( $"GitHubClient user not found: {exception}" );
        throw new ArgumentOutOfRangeException( nameof( owner ) );
      }
      catch( Exception exception )
      {
        this._logger.LogError( $"GitHubClient error: {exception}" );
        throw new Exception( "GitHub service not available" );
      }

      var result = userRepos?.Select( r => new GitHubRepo
      {
        Name = r.Name,
        StargazersCount = r.StargazersCount,
        WatchersCount = r.SubscribersCount,
        ForksCount = r.ForksCount,
        Size = r.Size
      } ) ?? Enumerable.Empty<GitHubRepo>( );

      return result;
    }
  }
}